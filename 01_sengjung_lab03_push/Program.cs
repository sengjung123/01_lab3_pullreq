﻿using System;

namespace _sengjung_lab03_push
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Type something:");
            string x = Console.ReadLine();
            Console.WriteLine("What you type was: "+ x);
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
            Console.WriteLine("This program shall be taken over by another being");
            Console.ReadKey();
        }
    }
}
